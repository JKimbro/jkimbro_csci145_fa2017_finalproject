import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class ConButI here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class ConButI extends Actor
{
    /**
     * Act - do whatever the ConButI wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        if( Greenfoot.mouseClicked(this))
        {
            Greenfoot.playSound("cont.wav");
            Selection select = new Selection();
            Greenfoot.setWorld(select);
        } // end if
    } // end act method
} // end ConButI class
