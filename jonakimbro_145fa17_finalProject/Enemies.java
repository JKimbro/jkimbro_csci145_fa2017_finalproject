import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Enemies walk back and forth on each floor trying to hit the player and the speed is 
 * determined by sex choice as well as progress  through the building
 * 
 * @author jkimbro@email.uscb.edu
 * @version 12.13.17
 */
public class Enemies extends Actor
{
    private boolean moveDirection = false;
    /**
     * Enemies Constructor to set movement direction
     */
    public Enemies()
    {
        // if(getX() < 400)
        // {
        // moveDirection = true; // set the initial move diection to forward
        // } // end if
        // else
        // {
        // moveDirection = false; // set the initial move diection to backwards
        // } // end if/else statement
    } // end Enemies constructor

    /**
     * Calls referenced methods
     */
    public void act() 
    {
        pacing();
    } // end act method

    /**
     * 
     */
    public void initSpeed()
    {

    }

    /**
     * Set AI movement control for player sex choice
     * 
     * Increase speed of enemies at certain floors for a certain sex
     */
    public void pacing()
    {
        if(ManCard.getManStatus() == true)
        {
            if (getY() >=250)
            {
                if( getX() >= 125 & getX() <= 670)
                {
                    int speed = Greenfoot.getRandomNumber(3) + 3;
                    if(moveDirection == true)
                    {
                        setLocation(getX() + speed, getY());
                    }
                    else if(moveDirection == false)
                    {
                        setLocation(getX() - speed, getY());
                    } // end triple nested if/else statment
                }
                else if(getX() < 125)
                {
                    moveDirection = true;
                    setLocation( 125, getY() );
                } 
                else
                {
                    moveDirection = false;
                    setLocation( 670, getY() );
                } // end double nested if/else
                             }
            else if(getY() < 250)
            {
                if( getX() >= 125 & getX() <= 670)
                {
                    int speed = Greenfoot.getRandomNumber(6) + 3;
                    if(moveDirection == true)
                    {
                        setLocation(getX() + speed, getY());
                    }
                    else if(moveDirection == false)
                    {
                        setLocation(getX() - speed, getY());
                    } // end triple nested if/else statment
                }
                else if(getX() < 125)
                {
                    moveDirection = true;
                    setLocation( 125, getY() );
                }
                else
                {
                    moveDirection = false;
                    setLocation( 670, getY() );
                } // end double nested if/else statement
            }
        }
        else if (WomanCard.getWomanStatus() == true)
        {
            if(getY() >= 250)
            {
                if( getX() >= 125 & getX() <= 670)
                {
                    int speed = Greenfoot.getRandomNumber(4) + 3;
                    if(moveDirection == true)
                    {
                        setLocation(getX() + speed, getY());
                    }
                    else if(moveDirection == false)
                    {
                        setLocation(getX() - speed, getY());
                    } // end triple nested if/else statment
                }
                else if(getX() < 125)
                {
                    moveDirection = true;
                    setLocation( 125, getY() );
                }
                else
                {
                    moveDirection = false;
                    setLocation( 670, getY() );
                }// end double nested if statement
            }
            else if(getY() < 250)
            {
                if( getX() >= 125 & getX() <= 670)
                {
                    int speed = Greenfoot.getRandomNumber(8) + 3;
                    if(moveDirection == true)
                    {
                        setLocation(getX() + speed, getY());
                    }
                    else if(moveDirection == false)
                    {
                        setLocation(getX() - speed, getY());
                    } // end riple nested if/else statment
                }
                else if(getX() < 125)
                {
                    moveDirection = true;
                    setLocation( 125, getY() );
                }
                else
                {
                    moveDirection = false;
                    setLocation( 670, getY() );
                } // end double nested if/else statement
            } // end nested if/else statement
        } // end if/else statement
    } // end pacing method
} // end Enemies class
