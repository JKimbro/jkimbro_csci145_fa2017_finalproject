import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class ConButS here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class ConButS extends Actor
{
    /**
     * Act - do whatever the ConBut wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        selectionMade();
    }

    public void selectionMade()
    {
        
        if((ManCard.getManStatus() == true)|(WomanCard.getWomanStatus() == true))
        {
            
            if(Greenfoot.mouseClicked(this))
            {
                Greenfoot.playSound("cont.wav");
                Sky sky = new Sky();
                Greenfoot.setWorld(sky);
            }
            
        }
        
    }
}
