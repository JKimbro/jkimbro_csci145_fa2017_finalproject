import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class StartButton here.
 * 
 * @author jkimbro@email.uscb.edu
 * @version 12.13.2017
 */
public class StartButton extends Actor
{
    
    /**
     * Act - do whatever the StartButton wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        
        if( Greenfoot.mouseClicked(this))
        {
            Greenfoot.playSound("jingle.wav");
            Info info = new Info();
            Greenfoot.setWorld(info);
        } // end if statement
        
    } // end act method
    
} // end StartButton class
