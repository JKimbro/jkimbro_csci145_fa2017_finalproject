import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This is the world where the user will select what sex they want to play as
 * 
 * @author Jkimbro@email.uscb.edu
 * @version 12.12.2017
 */
public class Selection extends World
{

    /**
     * Constructor for objects of class Selection.
     * 
     */
    public Selection()
    {    
        // Create a new world with 800x600 cells with a cell size of 1x1 pixels.
        super(800, 600, 1);
        prepare();
    }
    
    /**
     * Creates items to be instantiated by the constructor
     */
    public void prepare()
    {
        // Creates instances of objects to be instantiated ;)
        WomanCard woman = new WomanCard();
        ManCard man     = new ManCard();
        ChooseText ct   = new ChooseText();
        ConButS cb       = new ConButS();
        
        // Instantiates the instances of objects coded above 
        addObject(woman , 200, 300);
        addObject(man , 599, 300);
        addObject(ct , 400, 100);
        addObject(cb , 400, 550);
        
    }
    
}
