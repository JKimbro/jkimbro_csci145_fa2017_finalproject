import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Supercall for playable characters
 * 
 * @author jkimbro@email.uscb.edu
 * @version 12.14.17
 */
public class People extends Actor
{

    /**
     * Act - do whatever the People wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        // Add your action code here.
    }    

    /**
     * Method to be called by playable character subclass to set boundaries and
     * movement controls
     */
    public void movement(int speed)
    {
        if( getX() >= 115 & getX() <= 680)
        {
            if (Greenfoot.isKeyDown("up") & isTouching(Ladder.class))
            {
                /*if (isTouching(Ladder.class))*/
                //{
                Greenfoot.playSound("climb.wav");
                setLocation ( getX() , getY() - 60  );
                //} // end nested if statement
            }
            else if(Greenfoot.isKeyDown("up") & isTouching(LongLadder.class))
            {
                setLocation ( getX() , getY() - 120  );
            }
            else if (Greenfoot.isKeyDown("Left"))
            {
                setLocation ( getX() - speed , getY());
            }
            else if (Greenfoot.isKeyDown("right"))
            {
                setLocation ( getX() + speed , getY());
            } // end nested if-else Statement
            if(getY()< 528)
            {
                if(Greenfoot.isKeyDown("down"))
                {
                    Greenfoot.playSound("fall.wav");
                    setLocation ( getX() , 528 );
                } // end double nested if
            } // end nested if
        }
        else if (getX() < 115)
        {
            setLocation( 115, getY() );
        }
        else
        {
            setLocation( 680, getY() );
        } // end if else statement
    }

    // /**
    // * Checks to make sure the playable character is still in the playable area
    // */
    // public void isOnFloor()
    // {
    // if(isTouching(Floor.class))
    // {

    // }
    // else
    // {
    // GameOver go = new GameOver();
    // Greenfoot.setWorld(go);
    // } // end if statement
    // } // end isOnFloor method

    /**
     * Sets a condition for possible winning
     */
    public void winCondin()
    {
        if ( getY() <= 48)
        {
            Win win = new Win();
            Greenfoot.setWorld(win);
        } // end if
    } // end winCodin method
} // end People class

