import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class WomanHPM here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class WomanHPM extends Actor
{
    
    /**
     * Act - do whatever the WomanHPM wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        // Add your action code here.
    }    

    public void updateI(int hP)
    {
        if( hP == 2)
        {
            setImage("FemaleHearts.png");
        }
        else if ( hP == 1)
        {
            setImage("FemaleHearts1.png");
        }
        else if ( hP == 0)
        {
            setImage("FemaleHearts0.png");
        }
    }
}
