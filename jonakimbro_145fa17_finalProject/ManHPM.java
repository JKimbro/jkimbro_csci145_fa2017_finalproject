import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class ManHPM here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class ManHPM extends Actor
{
    
    /**
     * Act - do whatever the ManHPM wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        // Add your action code here.
    }   
    
    public void updateI(int hP)
    {
        if( hP == 3)
        {
            setImage("MaleHearts.png");
        }
        else if ( hP == 2)
        {
            setImage("MaleHearts2.png");
        }
        else if ( hP == 1)
        {
            setImage("MaleHearts1.png");
        }
        else if ( hP == 0)
        {
            setImage("MaleHearts0.png");
        }
    }
}
