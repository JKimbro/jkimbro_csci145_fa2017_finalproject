import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This is the male class where information will be called down form the
 * People superclass
 * 
 * @author Jkimbro 
 * @version 12.13.17
 */
public class Male extends People
{
    private int hP = 3;
    private int counter = 0;
    /**
     * Act - do whatever the Male wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        movement(7); // passes the int to the movement method defined in the 
        winCondin();
        health();
    } // end act method
    
    public int getHP()
    {
        return hP;
    } // end getHP
    
    public void setHP(int hP)
    {
        this.hP = hP;
    }
    
    /**
     * Health system
     */
    public void health()
    {
        if (isTouching(Enemies.class))
        {
            Greenfoot.playSound("hit.wav");
            setHP(getHP() - 1);
            resetSpawn();
            // getHP();
        } // end if
        if ( hP == 0)
        {
            counter++;
            if (counter == 50)
            {
                GameOver go = new GameOver();
                Greenfoot.setWorld(go);
            }
        } // end nested if
    }

    /**
     * Resets the location of the Man to the spawn point
     */
    public void resetSpawn()
    {
        setLocation(400, 528);
    } // end reset

    
    
} // end Male class
