import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class WomanCard here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class WomanCard extends PlayerCards
{
    /* FEILDS */
    private static boolean statusWoman = false;
    private static boolean statusMan = false;

    /**
     * Act - do whatever the WomanCard wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        selection();
    }    

    /**
     * sets womanStatus to update WomanCard
     */
    public static void setWomanStatus(boolean statusWoman)
    {
        WomanCard.statusWoman = statusWoman;
    }

    /**
     * Sets statusMan
     */
    public static void setManStatus(boolean statusMan)
    {
        WomanCard.statusMan = statusMan;
    }

    /**
     * set-up get/set methods to determine which card is selected
     */
    public static boolean getWomanStatus()
    {
        return WomanCard.statusWoman;
    }

    /**
     * return statusMan
     */
    public static boolean getManStatus()
    {
        return WomanCard.statusMan;
    }

    /**
     * Communicates with ManCard to set statusMan
     */
    public void selection()
    {
        if(Greenfoot.mouseClicked(this) & statusWoman == false)
        {
            Greenfoot.playSound("sel.wav");
            setImage("womanPortraitS.png"); 
            statusWoman = true;
            ManCard.setWomanStatus(true);
            statusMan = false;
            ManCard.setManStatus(false);
        }
        else if( ManCard.getManStatus() == true)
        {
            // statusWoman = false; // already implede if ManStatus == true
            setImage("womanPortrait.png");
        } // end if/else statement

    } // end selection method

} // end WomanCard class
