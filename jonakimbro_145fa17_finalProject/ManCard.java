import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class ManCard here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class ManCard extends PlayerCards
{
    /* FEILDS */
    private static boolean statusMan;
    private static boolean statusWoman;
    
    /**
     * Act - do whatever the ManCard wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        selection();
    }
    
    /**
     * sets womanStatus to update WomanCard
     */
    public static void setWomanStatus(boolean statusWoman)
    {
        ManCard.statusWoman = statusWoman;
    }
    
    
    /**
     * Sets statusMan
     */
    public static void setManStatus(boolean statusMan)
    {
        ManCard.statusMan = statusMan;
    }
    
    /**
     * set-up get methods to determine which card is selected
     */
    public static boolean getManStatus()
    {
        return ManCard.statusMan;
    }
    
    /**
     * set-up get methods to determine which card is selected
     */
    public static boolean getWomanStatus()
    {
        return ManCard.statusWoman;
    }
    
    public void selection()
    {
        if(Greenfoot.mouseClicked(this) & statusMan == false)
        {
            Greenfoot.playSound("sel.wav");
            setImage("manPortraitS.png");  
            statusMan = true;
            WomanCard.setManStatus(true);
            statusWoman = false;
            WomanCard.setWomanStatus(false);          
        }
        else if( WomanCard.getWomanStatus() == true)
        {
            // statusMan = false; // already implede if ManStatus == true
            setImage("manPortrait.png");
        }
    }
}
