import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Female here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Female extends People
{
    private int hP = 2;
    private int counter = 0;
    /**
     * Act - do whatever the Female wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        movement(5); // passes the int to the movement method defined in the 
                     // People superclass
        // calls the method defined in the People superclass
        winCondin();
        health();
        //send();
    } // end act method

    public int getHP()
    {
        return hP;
    } // end getHP
    
    public void setHP(int hP)
    {
        this.hP = hP;
    }
    
    /**
     * Health System
     */
    public void health()
    {
        if (isTouching(Enemies.class))
        {
            Greenfoot.playSound("hit.wav");
            setHP(getHP() - 1);
            resetSpawn();
        } // end if
        if ( this.hP == 0)
        {
            counter++;
            if (counter == 50)
            {
                GameOver go = new GameOver();
                Greenfoot.setWorld(go);
            } // end double nest if statement
        } // end nested if
    } // end health method

    /**
     * Resets the location of the Woman to the spawn point
     */
    public void resetSpawn()
    {
        setLocation(400, 528);
    } // end resetSpawn method

}// end Female class
