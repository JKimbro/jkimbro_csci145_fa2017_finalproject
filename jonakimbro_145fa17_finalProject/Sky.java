import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * A beautiful background of the sky.  objects are instantiated here.
 * 
 * @author jkimbro@email.uscb.edu
 * @version 12.08.2017
 */
public class Sky extends World
{
    Male male = new Male();
    Female female = new Female();
    /**
     * Constructor for objects of class Sky.
     * 
     */
    public Sky()
    {    
        // Create a new world with 800x600 cells with a cell size of 1x1 pixels.
        super(800, 600, 1); 

        prepare(); // call prepare method

        // Sets the game world to the Start World when the game is ran
        Start start = new Start();
        Greenfoot.setWorld(start);
    }

    public void act()
    {
        if(ManCard.getManStatus() == true)
        {
            ManHPM mhpbr = new ManHPM();
            addObject( mhpbr , 750 , 300);
            ManHPM mhpbl = new ManHPM();
            addObject( mhpbl , 49 , 300);
            mhpbr.updateI(male.getHP());
            mhpbl.updateI(male.getHP());
        }
        else
        {
            WomanHPM whpbr = new WomanHPM();
            addObject( whpbr , 750 , 300);
            WomanHPM whpbl = new WomanHPM();
            addObject( whpbl , 49 , 300);
            whpbr.updateI(female.getHP());
            whpbl.updateI(female.getHP());
        } // end if/else statement
    }

    /**
     * Instantiates the objects when the game is started up
     */
    public void prepare()
    {
        // Creates the playable game area
        Floor floor1 = new Floor();
        addObject(floor1, 400, 500);
        Floor floor2 = new Floor();
        addObject(floor2, 400, 380);
        Floor floor3 = new Floor();
        addObject(floor3, 400, 260);
        Floor floor4 = new Floor();
        addObject(floor4, 400, 140);
        Floor floor5 = new Floor();
        addObject(floor5, 400, 20);

        charChoSetup();
        // calls playerSelection
        playerSelection();
    } // end prepare

    /**
     * instantiates objects at locations based on the sex choose the play picked
     */
    public void charChoSetup()
    {// instantiates the ladders at the desired positions based on character choice
        if(ManCard.getManStatus() == true)
        {
            /* Add Enemies */
            Enemies enemy1 = new Enemies();
            addObject(enemy1 , 671,468);

            // Third Floor
            Enemies enemy2 = new Enemies();
            addObject(enemy2 , 125,408);

            // Fourth Floor
            Enemies enemy3 = new Enemies();
            addObject(enemy3 , 671,348);

            // Fifth Floor
            Enemies enemy4 = new Enemies();
            addObject(enemy4 , 125,288);

            // Sixth Floor
            Enemies enemy5 = new Enemies();
            addObject(enemy5 , 671,228);
            // Enemies enemy6 = new Enemies();
            // addObject(enemy6 , 125,228);

            // Seventh Floor
            // Enemies enemy7 = new Enemies();
            // addObject(enemy7 , 671,168);
            Enemies enemy8 = new Enemies();
            addObject(enemy8 , 125,168);

            // Eighth Floor
            Enemies enemy9 = new Enemies();
            addObject(enemy9 , 671,108);
            // Enemies enemy10 = new Enemies();
            // addObject(enemy10 , 125,108);

            /* Add Ladders */
            // First Floor
            if( Greenfoot.getRandomNumber(10) < 8)
            {
                Ladder ladder1 = new Ladder();
                addObject( ladder1, 200, 528);
            }
            else
            {
                LongLadder llr1 = new LongLadder();
                addObject( llr1, 200, 500);
            }// end if/else statement

            if( Greenfoot.getRandomNumber(10) < 8)
            {
                Ladder ladder2 = new Ladder();
                addObject( ladder2, 600, 528);
            }
            else
            {
                LongLadder llr2 = new LongLadder();
                addObject( llr2, 600, 500);
            } // end if/else statement

            // Second Floor
            Ladder ladder3 = new Ladder();
            addObject( ladder3, 350, 468);
            Ladder ladder4 = new Ladder();
            addObject( ladder4, 450, 468);

            // Third Floor
            if( Greenfoot.getRandomNumber(10) < 8)
            {
                Ladder ladder5 = new Ladder();
                addObject( ladder5, 200, 408);
            }
            else
            {
                LongLadder llr3 = new LongLadder();
                addObject(llr3 , 200 , 380);
            } // end if/else statement

            if( Greenfoot.getRandomNumber(10) < 8)
            {
                Ladder ladder6 = new Ladder();
                addObject( ladder6, 600, 408);
            }
            else
            {
                LongLadder llr4 = new LongLadder();
                addObject(llr4 , 600 , 380);
            } // end if/else statement

            // Forth Floor
            Ladder ladder7 = new Ladder();
            addObject( ladder7, 350, 348);
            Ladder ladder8 = new Ladder();
            addObject( ladder8, 450, 348);

            // Fifth Floor
            if(Greenfoot.getRandomNumber(25) + 1 > 15)
            {
                Ladder ladder9 = new Ladder();
                addObject( ladder9, Greenfoot.getRandomNumber(138) + 125, 288);
            }
            else if(Greenfoot.getRandomNumber(25) + 1 <= 10)
            {
                Ladder ladder10 = new Ladder();
                addObject( ladder10, Greenfoot.getRandomNumber(136) + 536, 288);
            }
            else
            {
                Ladder ladder9 = new Ladder();
                addObject( ladder9, Greenfoot.getRandomNumber(138) + 125, 288);
                Ladder ladder10 = new Ladder();
                addObject( ladder10, Greenfoot.getRandomNumber(136) + 536, 288);
            }

            // Sixth Floor
            if(Greenfoot.getRandomNumber(25) + 1 > 15)
            {
                Ladder ladder11 = new Ladder();
                addObject( ladder11, Greenfoot.getRandomNumber(138) + 262, 228);
            }
            else if(Greenfoot.getRandomNumber(25) + 1 <= 10)
            {
                Ladder ladder12 = new Ladder();
                addObject( ladder12, Greenfoot.getRandomNumber(136) + 400, 228);
            }
            else
            {
                Ladder ladder11 = new Ladder();
                addObject( ladder11, Greenfoot.getRandomNumber(138) + 262, 228);
                Ladder ladder12 = new Ladder();
                addObject( ladder12, Greenfoot.getRandomNumber(136) + 400, 228);

            } // end if/else statement

            // Seventh Floor
            if(Greenfoot.getRandomNumber(25) + 1 > 14)
            {
                Ladder ladder13 = new Ladder();
                addObject( ladder13, Greenfoot.getRandomNumber(138) + 125, 168);
            }
            else if(Greenfoot.getRandomNumber(25) + 1 <= 11)
            {
                Ladder ladder14 = new Ladder();
                addObject( ladder14, Greenfoot.getRandomNumber(136) + 536, 168);
            }
            else
            {
                Ladder ladder13 = new Ladder();
                addObject( ladder13, Greenfoot.getRandomNumber(138) + 125, 168);
                Ladder ladder14 = new Ladder();
                addObject( ladder14, Greenfoot.getRandomNumber(136) + 536, 168);

            } // end nested if/else method

            // Eigth Floor
            if(Greenfoot.getRandomNumber(10) + 1 % 3 == 0)
            {
                Ladder ladder16 = new Ladder();
                addObject( ladder16, Greenfoot.getRandomNumber(136) + 536, 108);
            }
            else if(Greenfoot.getRandomNumber(10) + 1 % 2 == 0)
            {
                Ladder ladder15 = new Ladder();
                addObject( ladder15, Greenfoot.getRandomNumber(138) + 125, 108);
            }
            else
            {
                Ladder ladder15 = new Ladder();
                addObject( ladder15, Greenfoot.getRandomNumber(138) + 125, 108);
                Ladder ladder16 = new Ladder();
                addObject( ladder16, Greenfoot.getRandomNumber(136) + 536, 108);

            } // end if / else statement

        }
        else if (WomanCard.getWomanStatus() == true)
        {
            /* Add Enemies */
            // Second Floor
            Enemies enemy1 = new Enemies();
            addObject(enemy1 , 671,468);

            // Third Floor
            Enemies enemy2 = new Enemies();
            addObject(enemy2 , 125,408);

            // Fourth Floor
            Enemies enemy3 = new Enemies();
            addObject(enemy3 , 671,348);

            // Fifth Floor
            Enemies enemy4 = new Enemies();
            addObject(enemy4 , 125,288);

            // Sixth Floor
            Enemies enemy5 = new Enemies();
            addObject(enemy5 , 671,228);
            // Enemies enemy6 = new Enemies();
            // addObject(enemy6 , 125,228);

            // Seventh Floor
            // Enemies enemy7 = new Enemies();
            // addObject(enemy7 , 671,168);
            Enemies enemy8 = new Enemies();
            addObject(enemy8 , 125,168);

            // Eighth Floor
            Enemies enemy9 = new Enemies();
            addObject(enemy9 , 671,108);
            // Enemies enemy10 = new Enemies();
            // addObject(enemy10 , 125,108);

            /* Add Ladders */
            // First Floor
            Ladder ladder1 = new Ladder();
            addObject( ladder1, 200, 528);
            Ladder ladder2 = new Ladder();
            addObject( ladder2, 600, 528);
            // Second Floor
            Ladder ladder3 = new Ladder();
            addObject( ladder3, 350, 468);
            Ladder ladder4 = new Ladder();
            addObject( ladder4, 450, 468);
            // Third Floor
            Ladder ladder5 = new Ladder();
            addObject( ladder5, 200, 408);
            Ladder ladder6 = new Ladder();
            addObject( ladder6, 600, 408);
            // Forth Floor
            Ladder ladder7 = new Ladder();
            addObject( ladder7, 350, 348);
            Ladder ladder8 = new Ladder();
            addObject( ladder8, 450, 348);

            // Fifth Floor
            if(Greenfoot.getRandomNumber(25) + 1 > 15)
            {
                Ladder ladder9 = new Ladder();
                addObject( ladder9, Greenfoot.getRandomNumber(138) + 125, 288);
            }
            else if(Greenfoot.getRandomNumber(25) + 1 <= 10)
            {
                Ladder ladder10 = new Ladder();
                addObject( ladder10, Greenfoot.getRandomNumber(136) + 536, 288);
            }
            else
            {
                Ladder ladder9 = new Ladder();
                addObject( ladder9, Greenfoot.getRandomNumber(138) + 125, 288);
                Ladder ladder10 = new Ladder();
                addObject( ladder10, Greenfoot.getRandomNumber(136) + 536, 288);
            }

            // Sixth Floor
            if(Greenfoot.getRandomNumber(25) + 1 > 15)
            {
                Ladder ladder11 = new Ladder();
                addObject( ladder11, Greenfoot.getRandomNumber(138) + 262, 228);
            }
            else if(Greenfoot.getRandomNumber(25) + 1 <= 10)
            {
                Ladder ladder12 = new Ladder();
                addObject( ladder12, Greenfoot.getRandomNumber(136) + 400, 228);
            }
            else
            {
                Ladder ladder11 = new Ladder();
                addObject( ladder11, Greenfoot.getRandomNumber(138) + 262, 228);
                Ladder ladder12 = new Ladder();
                addObject( ladder12, Greenfoot.getRandomNumber(136) + 400, 228);
            } // end if/else statement

            // Seventh Floor
            if(Greenfoot.getRandomNumber(25) + 1 >= 13)
            {
                Ladder ladder13 = new Ladder();
                addObject( ladder13, Greenfoot.getRandomNumber(138) + 125, 168);
            }
            else
            {
                Ladder ladder14 = new Ladder();
                addObject( ladder14, Greenfoot.getRandomNumber(136) + 536, 168);
            } // end nested if/else method

            // Eigth Floor
            if(Greenfoot.getRandomNumber(10) + 1 % 2 == 0)
            {
                Ladder ladder15 = new Ladder();
                addObject( ladder15, Greenfoot.getRandomNumber(138) + 125, 108);
            }
            else
            {
                Ladder ladder16 = new Ladder();
                addObject( ladder16, Greenfoot.getRandomNumber(136) + 536, 108);
            } // end if / else statement
        } // end else/if method
    } // end charChoSetup method

    /**
     * Choose which playable character to instantiate based on what the user choose
     */
    public void playerSelection()
    {
        if(ManCard.getManStatus() == true)
        {
            addObject(male , 400, 528);
            ManHPM hpbr = new ManHPM();
            addObject( hpbr , 750 , 300);
            ManHPM whpl = new ManHPM();
            addObject( whpl , 49 , 300);
        }
        else
        {
            addObject(female , 400, 528);
            WomanHPM hpbr = new WomanHPM();
            addObject( hpbr , 750 , 300);
            WomanHPM hpbl = new WomanHPM();
            addObject( hpbl , 49 , 300);
        } // end if/else statement
    } // end playerSelection method

    public void healthSys()
    {
        if(ManCard.getManStatus() == true)
        {
            ManHPM hpbr = new ManHPM();
            addObject( hpbr , 750 , 300);
            ManHPM hpbl = new ManHPM();
            addObject( hpbl , 49 , 300);
        }
        else
        {
            WomanHPM hpbr = new WomanHPM();
            addObject( hpbr , 750 , 300);
            WomanHPM hpbl = new WomanHPM();
            addObject( hpbl , 49 , 300);
        } // end if/else statement
    }
} // end Sky class
